import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final Function restart;

  Result(this.restart);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            'Well Done',
            style: TextStyle(
              fontSize: 30.0,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: RaisedButton(
            color: Colors.green,
            onPressed: restart,
            child: Text('Restart'),
          ),
        )
      ],
    );
  }
}
